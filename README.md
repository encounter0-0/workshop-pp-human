# 【校园AI Day-AI workshop】基于PP-HUMAN的客流及属性统计
 **_本仓库中上传的是PaddleDetection/deploy/pphuman文件夹下的文件，使用时需要基于PaddleDetection部署后的环境进行pp-human文件夹的替换_** 

 **（即：将本项目作为pphuman文件夹替换PaddleDetection文件夹中的pphuman文件夹）** 

[ **Ai studio项目传送门** ](https://aistudio.baidu.com/aistudio/projectdetail/4172972?contributionType=1&shared=1)

**本地演示视频**


[**视频传送门**](https://www.bilibili.com/video/BV1hZ4y147qg?share_source=copy_web)

## 一、项目背景
社区是城市的关键组成部分，社区治理是围绕社区场景下的人、地、物、情、事的管理与服务。

随着城市化的快速推进及人口流动的快速增加，传统社区治理在人员出入管控、安防巡逻、车辆停放管理等典型场景下都面临着人力不足、效率低下、响应不及时等诸多难题。而人工智能技术代替人力，实现人、车、事的精准治理，大幅降低人力、物质、时间等成本，以最低成本发挥最强大的管理效能，有效推动城市治理向更“数字化、自动化、智慧化”的方向演进。

传统社区视频监控80%都依靠人工实现，随着摄像头在社区中的大规模普及，**日超千兆的视频图像数据、人员信息的日渐繁杂已远超人工的负荷**。

### 1.1 技术实现
为解决以上问题，我们灵活应用飞桨行人分析**PP-Human中的行人检测和属性分析模型**，实时识别行人的**性别、年龄、衣着打扮等26种属性并记录统计**。

PP-Human相关配置位于`deploy/pphuman/config/infer_cfg.yml`中，存放模型路径，完成不同功能需要设置不同的任务类型。

功能及任务类型对应表单如下：

| 输入类型 | 功能 | 任务类型 | 配置项 |
|-------|-------|----------|-----|
| 图片 | 属性识别 | 目标检测 属性识别 | DET ATTR |
| 单镜头视频 | 属性识别 | 多目标跟踪 属性识别 | MOT ATTR |
| 单镜头视频 | 行为识别 | 多目标跟踪 关键点检测 行为识别 | MOT KPT ACTION |

**本项目应用到的是**：
- 社区人员信息留存：单镜头视频或图片输入的属性识别

其中，属性分析包含26种不同属性：
```
- 性别：男、女
- 年龄：小于18、18-60、大于60
- 朝向：朝前、朝后、侧面
- 配饰：眼镜、帽子、无
- 正面持物：是、否
- 包：双肩包、单肩包、手提包
- 上衣风格：带条纹、带logo、带格子、拼接风格
- 下装风格：带条纹、带图案
- 短袖上衣：是、否
- 长袖上衣：是、否
- 长外套：是、否
- 长裤：是、否
- 短裤：是、否
- 短裙&裙子：是、否
- 穿靴：是、否
```

<div align="center">
  <img src="https://user-images.githubusercontent.com/48054808/159898428-5bda0831-7249-4889-babd-9165f26f664d.gif" width="700"/>
  <br>
  街道人员属性识别
</div>


**PP-Human**是基于飞桨深度学习框架的业界首个开源的实时行人分析工具，具有功能丰富，应用广泛和部署高效三大优势。PP-Human 支持图片/单镜头视频/多镜头视频多种输入方式，功能覆盖多目标跟踪、属性识别和行为分析。能够广泛应用于智慧交通、智慧社区、工业巡检等领域。支持服务器端部署及TensorRT加速，T4服务器上可达到实时。
<div align="center">
  <img src="https://user-images.githubusercontent.com/22989727/163980774-2fc2f67f-5c48-4b7c-990a-4ba61fd1c5c5.png" width="700"/>
</div>

**详细文档可参考：https://github.com/PaddlePaddle/PaddleDetection/tree/develop/deploy/pphuman**

### 1.2 技术方案

**在本项目中，我们基于PP-HUMAN实现一套行人客流和人员属性统计系统，可以通过数据库查询，有可视化统计页面。**

<div align="center">
  <img src="https://ai-studio-static-online.cdn.bcebos.com/97cbe09ddb4d44799f62169bb023e069b1346cbcc66e45a5a20838058256e61b" width="700"/>
</div>

>1.2.1.识别：
 >>   使用PP-HUMAN的行人属性检测方案，对于视频流的行人属性识别需要用到MOT（多目标跟踪）模型和ATTR（行人属性识别）模型，**由MOT模型识别并跟踪行人，由ATTR模型进行多分类识别行人属性**。
 
>1.2.2.后端：
>> Flask是一个轻量级的可定制框架，使用Python语言编写，较其他同类型框架更为灵活、轻便、安全且容易上手。它可以很好地结合MVC模式进行开发，开发人员分工合作，小型团队在短时间内就可以完成功能丰富的中小型网站或Web服务的实现。另外，Flask还有很强的定制性，用户可以根据自己的需求来添加相应的功能，在保持核心功能简单的同时实现功能的丰富与扩展，其强大的插件库可以让用户实现个性化的网站定制，开发出功能强大的网站。（[百度百科](https://baike.baidu.com/item/Flask/1241509)）
**总而言之flask用python编写，对于技术栈还不够全面和足够熟练的新手小队实现起来方便一些。**

> 1.2.3.前端：
    >>1.2.3.1.Ajax：
    >>> Asynchronous Javascript And XML（异步JavaScript和XML）是一种web数据交互方式，使用Ajax技术网页应用能够快速地将增量更新呈现在用户界面上，而不需要重载（刷新）整个页面，这使得程序能够更快地回应用户的操作。**用来实时刷新数据**。
    >
    >>1.2.3.2.ECharts：
    >>>ECharts是一款基于JavaScript的数据可视化图表库，提供直观，生动，可交互，**可个性化定制的数据可视化图表**。
    
> 1.2.4.数据库：
>> SQLite是一款轻型的数据库，是遵守ACID的关系型数据库管理系统，它包含在一个相对小的C库中。它的设计目标是嵌入式的，而且已经在很多嵌入式产品中使用了它，它占用资源非常的低，在嵌入式设备中，可能只需要几百K的内存就够了。技术成熟、资源占用少并且可以直接在python中import直接使用，**有SQL知识基础的小伙伴上手很快**。



#### 安装教程


##### 部署踩坑指南：
(记录了一些踩的坑，菜菜遇到坑只会搜索)

1.[: cannot connect to X server 
!](https://ai-studio-static-online.cdn.bcebos.com/7d5b131b464d4e3ea437a5951c294d0771cf0980bab848f1a2f9bd6b37da1867)

原因：在BML CodeLab中没有允许实时显示可视化视频的权限，故在BML CodeLab中只能注释掉PaddleDetection/deploy/pphuman/pipeline.py里的
```
#在BML CodeLab中无法实时显示，故注释下面三行
cv2.imshow('PPHuman', im)
if cv2.waitKey(1) & 0xFF == ord('q'):
    break
```
ctrl+f搜索，将两个位置的上面三行注释后再次运行即可运行对视频的检测。（本地的打开显示预测处理结果是没有问题的）    

2.[重新annaconda配置环境](http://t.csdn.cn/Vs3Kv)

3.[遇到报错ERROR: Failed building wheel for pycocotools](http://t.csdn.cn/KFlsz)

4.[发现系统没有cudnn，配置教程](http://t.csdn.cn/A6Oxb)

5.[cudnn库报错缺dll文件](http://t.csdn.cn/vVe3y)

6.[mysql主键唯一键重复插入解决方法](http://t.csdn.cn/iqK7u)

7.[视频流传输](https://www.cnblogs.com/arkenstone/p/7159615.html)

#### 使用说明


在PaddleDetection/文件夹上级路径下执行命令行：
1.识别模块
```
#补充安装相关库（不然运行下面的）视频行人属性识别指令会报错缺库
pip install motmetrics
pip install lap
#视频行人属性识别
#pipeline.py里的attr_res是视野里识别出的标签
python PaddleDetection/deploy/pphuman/pipeline.py \
    --config deploy/pphuman/config/infer_cfg.yml \
    --model_dir mot=output_inference/mot_ppyoloe_l_36e_pipeline/ attr=output_inference/strongbaseline_r50_30e_pa100k/ \
    --video_file=/home/aistudio/属性.mp4 \
    --enable_attr=True \
    --device=gpu
```
##### 预测效果
![](https://ai-studio-static-online.cdn.bcebos.com/aecf0bfb7cd247cc8978f23db615c355a8fa46ec49764527b86e9b9528e129d3)

2.运行后端服务
```
python PaddleDetection/deploy/pphuman/app.py
```
点击后端服务启动后提供的链接查看效果。
![](https://ai-studio-static-online.cdn.bcebos.com/b7d48ed9171e4fd0b80d258b58d811448c0dbde0667f4eba8b0dd08d7e5d4c4a)
![](https://ai-studio-static-online.cdn.bcebos.com/c5e3ac1a2b27400f8e26a188a945ec6adb459e381efa43f8851a2e3c3bf503f2)

#### 参与贡献

- 本项目完成基于PP-Human，存储数据到历史数据库，同时能够将实时检测结果推到前端页面，实现了一套行人客流和人员属性统计系统，可以通过数据库查询，有可视化统计页面。

- 本次【校园AI Day-AI workshop】活动受益匪浅，不仅小组成员共同协作感受到了深度学习的乐趣，也对**PP-Human**有了更熟悉的了解，为今后的学习研究打下良好的基础。

### 致谢
 - 感谢百度飞桨能够提供这样的机会让我们有一个平台实践尝试完成项目，也非常感谢本项目``` 胡雷``` 导师的悉心指导与关注。每次遇到困难及报错的时候，导师的耐心指导极大的拓宽了我们的视野与思路，让我们有有效解决问题的探索方向。

> 队伍名称：新手村炼丹小队
>> 队长：[Encounter0_0](https://aistudio.baidu.com/aistudio/personalcenter/thirdview/545541)
>> 队员：[啊Qdai 一](https://aistudio.baidu.com/aistudio/personalcenter/thirdview/1924772)
、[善良的呵呵大1](https://aistudio.baidu.com/aistudio/personalcenter/thirdview/591799)、小薛、羊羔